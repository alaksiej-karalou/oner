package com.gitlab.aliakseikaraliou.oner.base.utils

import org.junit.Assert.*
import org.junit.Test

class PhoneNumberUtilsTest{
    @Test
    fun isValid() {
        assertTrue(PhoneNumberUtils.isValid("+375336837494"))
        assertTrue(PhoneNumberUtils.isValid("375336837494"))
        assertFalse(PhoneNumberUtils.isValid("3753t36837494"))
        assertFalse(PhoneNumberUtils.isValid("+375"))
        assertFalse(PhoneNumberUtils.isValid("+375336837494375336837494"))
    }
}