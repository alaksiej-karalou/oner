package com.gitlab.aliakseikaraliou.oner.base.utils

import android.app.Application
import com.facebook.stetho.Stetho
import com.squareup.leakcanary.LeakCanary

object LibUtils {
    fun init(application: Application) {
        Stetho.initializeWithDefaults(application)
        LeakCanary.install(application)
    }
}