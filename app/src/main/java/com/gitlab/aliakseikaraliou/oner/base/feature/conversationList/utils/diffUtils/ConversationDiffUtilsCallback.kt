package com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.utils.diffUtils

import androidx.recyclerview.widget.DiffUtil
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation

class ConversationDiffUtilsCallback(private val old: List<Conversation>,
                                    private val new: List<Conversation>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = old[oldItemPosition].id == new[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = old[oldItemPosition] == new[newItemPosition]

    override fun getOldListSize() = old.size

    override fun getNewListSize() = new.size
}