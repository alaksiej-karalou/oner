package com.gitlab.aliakseikaraliou.oner.sms.models.clean

import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.base.models.ConversationModel
import com.gitlab.aliakseikaraliou.oner.sms.db.repositories.SmsConversation

class SmsConversationModel(override val conversations: List<SmsConversation>)
    : ConversationModel
