package com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.base.models.Author

interface VkAuthor : Author, VkReceiver