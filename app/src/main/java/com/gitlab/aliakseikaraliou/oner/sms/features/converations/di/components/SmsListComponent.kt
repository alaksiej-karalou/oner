package com.gitlab.aliakseikaraliou.oner.sms.features.converations.di.components

import com.gitlab.aliakseikaraliou.oner.sms.features.converations.di.modules.SmsConversationListModule
import com.gitlab.aliakseikaraliou.oner.sms.features.converations.di.scopes.SmsListScope
import com.gitlab.aliakseikaraliou.oner.sms.features.converations.ui.view.SmsConversationListFragment
import dagger.BindsInstance
import dagger.Subcomponent

@SmsListScope
@Subcomponent(modules = [SmsConversationListModule::class])
interface SmsListComponent {
    fun inject(fragment: SmsConversationListFragment)
    
    @Subcomponent.Builder
    interface Builder {
        @BindsInstance
        fun fragment(fragment: SmsConversationListFragment): Builder
        
        fun build(): SmsListComponent
    }
}