package com.gitlab.aliakseikaraliou.oner.base.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {
    abstract val layoutId: Int
    open val themeId: Int? = null

    val baseActivity
        get() = activity as BaseActivity

    final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layoutInflater = themeId?.let { themeId ->
            context?.theme?.applyStyle(themeId, true)

            val contextThemeWrapper = ContextThemeWrapper(activity, themeId)
            inflater.cloneInContext(contextThemeWrapper)
        } ?: inflater

        return layoutInflater.inflate(layoutId, container, false)
    }
}