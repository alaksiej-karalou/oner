package com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.viewModels

import androidx.lifecycle.LiveData
import com.gitlab.aliakseikaraliou.oner.base.source.Source

interface BaseConversationListViewModel<T> {
    val liveData: LiveData<Source<T>>

    fun loadConversations()
}