package com.gitlab.aliakseikaraliou.oner.vk.network.interceptors

import com.gitlab.aliakseikaraliou.oner.vk.VkConfiguration
import com.gitlab.aliakseikaraliou.oner.vk.VkConstants
import okhttp3.Interceptor
import okhttp3.Response

class VkQueryInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
    
        val url = request.url()
                .newBuilder()
                .addQueryParameter(VkConstants.Queries.ACCESS_TOKEN, VkConfiguration.ACCESS_TOKEN)
                .addQueryParameter(VkConstants.Queries.VERSION, VkConfiguration.API_VERSION.toString())
                .build()
    
        Thread.sleep(VkConfiguration.DELAY_IN_MILLIS)
        
        val updatedRequest = request.newBuilder()
                .url(url)
                .build()
    
        return chain.proceed(updatedRequest)
    }
    
}