package com.gitlab.aliakseikaraliou.oner.telegram.execution.executables

import com.gitlab.aliakseikaraliou.oner.telegram.converters.TelegramConversationConverter
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations.TelegramDraftConversation
import org.drinkless.td.libcore.telegram.TdApi

class TelegramConversationExecutable(chatId: Long) : BaseTelegramExecutable<TdApi.Chat, TelegramDraftConversation>() {
    override val function = TdApi.GetChat(chatId)
    override fun convertData(it: TdApi.Chat) = TelegramConversationConverter.convert(it)
}
