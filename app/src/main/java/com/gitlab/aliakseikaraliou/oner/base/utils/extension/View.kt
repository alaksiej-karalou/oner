package com.gitlab.aliakseikaraliou.oner.base.utils.extension

import android.view.View

var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        if (value) {
            visibility = View.VISIBLE
        } else {
            visibility = View.GONE
        }
    }
