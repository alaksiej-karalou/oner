package com.gitlab.aliakseikaraliou.oner.base.models

interface User : Author {
    val firstName: String
    val lastName: String
    val username: String?
    val isOnline: Boolean
        get() = isOnlineMobile || isOnlineWeb
    val isOnlineMobile: Boolean
    val isOnlineWeb: Boolean

    override val fullName
        get() = "$firstName $lastName"

}