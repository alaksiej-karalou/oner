package com.gitlab.aliakseikaraliou.oner.base.utils.extension

import java.util.*

fun Date.toCalendar() = Calendar.getInstance().apply {
    time = this@toCalendar
}