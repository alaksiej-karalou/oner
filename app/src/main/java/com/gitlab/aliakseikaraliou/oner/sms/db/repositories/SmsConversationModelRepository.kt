package com.gitlab.aliakseikaraliou.oner.sms.db.repositories

import android.content.Context
import android.database.Cursor
import android.net.Uri
import com.gitlab.aliakseikaraliou.oner.base.source.Source
import com.gitlab.aliakseikaraliou.oner.base.utils.extension.get
import com.gitlab.aliakseikaraliou.oner.base.utils.extension.toMap
import com.gitlab.aliakseikaraliou.oner.sms.models.clean.SmsConversationModel
import com.gitlab.aliakseikaraliou.oner.sms.models.clean.SmsMessage
import com.gitlab.aliakseikaraliou.oner.sms.models.clean.authors.SmsAuthor
import com.gitlab.aliakseikaraliou.oner.sms.models.clean.authors.SmsChannel
import com.gitlab.aliakseikaraliou.oner.sms.models.draft.SmsDraftConversation
import com.gitlab.aliakseikaraliou.oner.sms.models.draft.SmsDraftConversationModel
import com.gitlab.aliakseikaraliou.oner.sms.models.draft.SmsDraftMessage
import java.util.Date

class SmsConversationModelRepository(private val context: Context,
                                     private val smsContactRepository: SmsContactRepository) {

    private val smsUri = Uri.parse("content://sms/")

    suspend fun loadConversations(offset: Int = 0, count: Int): Source<SmsConversationModel> {
        return context.contentResolver
                .query(smsUri,
                        null,
                        null,
                        null,
                        "$DATE DESC")
                ?.takeIf { it.moveToFirst() }
                ?.use { cursor ->
                    val draftConversationModel = SmsDraftConversationModel()

                    do {
                        parseMessage(cursor)?.let { draftMessage ->
                            draftConversationModel.put(draftMessage)
                        }

                    } while (cursor.moveToNext())

                    val conversationModel = convert(draftConversationModel.subList(offset, count))
                    Source.success(conversationModel)
                }?:Source.error()
    }

    private fun convert(draftMessages: List<SmsDraftConversation>): SmsConversationModel {
        val conversations = draftMessages.map { smsDraftConversation ->

            val author = smsDraftConversation.lastMessage.address.toLongOrNull()?.let { phoneNumber ->
                smsContactRepository.loadContactByNumber(phoneNumber)
            } ?: SmsChannel(id = smsDraftConversation.threadId.toLong(),
                    fullName = smsDraftConversation.lastMessage.address)

            val message = smsDraftConversation.lastMessage.create(author)

            SmsConversation(receiver = author,
                    threadId = smsDraftConversation.threadId,
                    lastMessage = message,
                    unreadCount = smsDraftConversation.unreadCount,
                    totalCount = smsDraftConversation.totalCount)
        }

        return SmsConversationModel(conversations)
    }

    private fun parseMessage(cursor: Cursor): SmsDraftMessage? {
        return try {
            val toMap = cursor.toMap()

            val id = cursor.get<Long>(ID)
            val threadId = cursor.get<Int>(THREAD_ID)
            val address = cursor.get<String>(ADDRESS)
            val date = Date(cursor.get<Long>(DATE))
            val text = cursor.get<String>(BODY)
            val isRead = cursor.get<Boolean>(READ)

            return SmsDraftMessage(id = id,
                    threadId = threadId,
                    address = address,
                    date = date,
                    text = text,
                    isRead = isRead,
                    isOut = false)

        } catch (e: TypeCastException) {
            null
        }
    }

    companion object {
        private const val ID = "_id"
        private const val ADDRESS = "address"
        private const val BODY = "body"
        private const val DATE = "date"
        private const val THREAD_ID = "thread_id"
        private const val READ = "read"
    }
}