package com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.base.models.Channel

data class TelegramChannel(override val id: Long,
                           override val fullName: String,
                           override val imageUrl: String?) : Channel, TelegramAuthor