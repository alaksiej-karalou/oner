package com.gitlab.aliakseikaraliou.oner.sms.features.converations.dataSource

import androidx.paging.DataSource
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.sms.db.repositories.SmsConversationModelRepository

class SmsConversationModelDataSourceFactory(val repository: SmsConversationModelRepository) : DataSource.Factory<Int, Conversation>() {

    private val dataSource by lazy {
        SmsConversationModelDataSource(repository)
    }

    override fun create() = SmsConversationModelDataSource(repository)
}