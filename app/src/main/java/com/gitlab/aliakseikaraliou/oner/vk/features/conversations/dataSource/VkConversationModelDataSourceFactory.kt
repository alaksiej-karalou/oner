package com.gitlab.aliakseikaraliou.oner.vk.features.conversations.dataSource

import androidx.paging.DataSource
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.VkConversationModelRepository

class VkConversationModelDataSourceFactory(val repository: VkConversationModelRepository) : DataSource.Factory<Int, Conversation>() {

    private val dataSource by lazy {
        VkConversationModelDataSource(repository)
    }

    override fun create() = VkConversationModelDataSource(repository)
}