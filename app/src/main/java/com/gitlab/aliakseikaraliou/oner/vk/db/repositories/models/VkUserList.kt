package com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models

import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkUser

class VkUserList(val list: List<VkUser>)
