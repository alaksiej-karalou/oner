package com.gitlab.aliakseikaraliou.oner.base.di.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope