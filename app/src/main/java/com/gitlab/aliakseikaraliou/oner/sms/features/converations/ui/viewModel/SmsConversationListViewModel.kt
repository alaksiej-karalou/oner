package com.gitlab.aliakseikaraliou.oner.sms.features.converations.ui.viewModel

import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.viewModels.ConversationListPagedViewModel
import com.gitlab.aliakseikaraliou.oner.base.source.Source
import com.gitlab.aliakseikaraliou.oner.sms.features.converations.dataSource.SmsConversationModelDataSourceFactory
import com.gitlab.aliakseikaraliou.oner.sms.models.clean.SmsConversationModel
import com.gitlab.aliakseikaraliou.oner.vk.VkConfiguration

class SmsConversationListViewModel(dataSourceFactory: SmsConversationModelDataSourceFactory) : ConversationListPagedViewModel(dataSourceFactory) {
    //TODO fix
    override val config: PagedList.Config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(VkConfiguration.INITIAL_PAGE_SIZE)
            .setPageSize(VkConfiguration.PAGE_SIZE)
            .build()

    private val progressLiveData = dataSourceFactory.create().liveData
    private val observer = Observer<Source<SmsConversationModel>> { presentationalModel ->
        presentationalModel?.let { liveData.postValue(it) }
    }

    override fun loadConversations() {
        liveData.addSource(progressLiveData, observer)

        super.loadConversations()
    }

    override fun onCleared() {
        liveData.removeSource(progressLiveData)

        super.onCleared()
    }
}