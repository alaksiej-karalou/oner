package com.gitlab.aliakseikaraliou.oner.base.di.modules

import android.content.Context
import com.gitlab.aliakseikaraliou.oner.base.db.BaseRepositoryFactory
import com.gitlab.aliakseikaraliou.oner.base.di.scopes.AppScope
import com.gitlab.aliakseikaraliou.oner.sms.db.SmsRepositoryFactory
import com.gitlab.aliakseikaraliou.oner.telegram.db.TelegramRepositoryFactory
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.VkRepositoryFactory
import com.gitlab.aliakseikaraliou.oner.vk.BaseVkModel
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @AppScope
    @Provides
    fun provideTelegramRepositoryFactory(): TelegramRepositoryFactory {
        return TelegramRepositoryFactory()
    }

    @AppScope
    @Provides
    fun provideSmsRepositoryFactory(context: Context): SmsRepositoryFactory {
        return SmsRepositoryFactory(context)
    }

    @AppScope
    @Provides
    fun provideVkRepositoryFactory(vkModel: BaseVkModel): VkRepositoryFactory {
        return VkRepositoryFactory(vkModel)
    }

    @AppScope
    @Provides
    fun provideBaseRepositoryFactory(telegramRepositoryFactory: TelegramRepositoryFactory,
                                     smsRepositoryFactory: SmsRepositoryFactory,
                                     vkRepositoryFactory: VkRepositoryFactory): BaseRepositoryFactory {
        return BaseRepositoryFactory(telegramRepositoryFactory, smsRepositoryFactory, vkRepositoryFactory)
    }
}