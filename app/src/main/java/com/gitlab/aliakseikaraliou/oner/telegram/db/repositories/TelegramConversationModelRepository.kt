package com.gitlab.aliakseikaraliou.oner.telegram.db.repositories

import com.gitlab.aliakseikaraliou.oner.telegram.execution.executables.TelegramChatIdsExecutable
import com.gitlab.aliakseikaraliou.oner.telegram.execution.executables.TelegramConversationExecutable
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramChatKey
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramConversationModel
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.conversations.TelegramChannelConversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.conversations.TelegramChatConversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.conversations.TelegramPrivateConversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations.TelegramChannelDraftConversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations.TelegramChatDraftConversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations.TelegramPrivateDraftConversation

class TelegramConversationModelRepository(private val userRepository: TelegramUserRepository,
                                          private val channelRepository: TelegramChannelRepository,
                                          private val chatRepository: TelegramChatRepository) {

    suspend fun loadConversations(key: TelegramChatKey = TelegramChatKey.default(), count: Int): TelegramConversationModel {
        val chatsExecutable = TelegramChatIdsExecutable(count, key)

        val conversations = chatsExecutable.execute().mapNotNull { chatId ->
            val executable = TelegramConversationExecutable(chatId)
            val draftConversation = executable.execute()

            when (draftConversation) {
                is TelegramPrivateDraftConversation -> loadPrivateData(draftConversation)
                is TelegramChannelDraftConversation -> loadChannelData(draftConversation)
                is TelegramChatDraftConversation -> loadChatData(draftConversation)
                else -> throw IllegalArgumentException()
            }
        }

        return TelegramConversationModel(conversations)
    }

    private suspend fun loadPrivateData(draft: TelegramPrivateDraftConversation): TelegramPrivateConversation {
        val telegramUser = userRepository.loadUser(draft.authorId)

        return draft.convert(telegramUser)
    }

    private suspend fun loadChannelData(draft: TelegramChannelDraftConversation): TelegramChannelConversation {
        val telegramChannel = channelRepository.loadChannelById(draft.authorId)

        return draft.convert(telegramChannel)
    }

    private suspend fun loadChatData(draft: TelegramChatDraftConversation): TelegramChatConversation? {
        val telegramUser = draft.lastMessage?.authorId?.let { userRepository.loadUser(it) }

        return draft.lastMessage?.chatId?.let {
            val telegramChat = chatRepository.loadChat(it)
            draft.build(telegramChat, telegramUser)
        }

    }

}