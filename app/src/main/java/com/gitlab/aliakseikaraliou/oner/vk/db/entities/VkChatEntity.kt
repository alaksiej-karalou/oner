package com.gitlab.aliakseikaraliou.oner.vk.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkChat

@Entity(tableName = "chats")
data class VkChatEntity(@PrimaryKey val id: Long,
                        val fullName: String,
                        val imageUrl: String?) {

    constructor(chat: VkChat) : this(id = chat.id,
            fullName = chat.fullName,
            imageUrl = chat.imageUrl)

}