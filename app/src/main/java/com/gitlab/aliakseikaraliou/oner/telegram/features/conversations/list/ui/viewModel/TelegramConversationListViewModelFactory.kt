package com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.ui.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.dataSource.TelegramConversationModelDataSourceFactory

@Suppress("UNCHECKED_CAST")
class TelegramConversationListViewModelFactory(
        private val dataSourceFactory: TelegramConversationModelDataSourceFactory) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>) = when (modelClass) {
        TelegramConversationListViewModel::class.java -> TelegramConversationListViewModel(dataSourceFactory)
        else -> throw IllegalArgumentException()
    } as T
}
