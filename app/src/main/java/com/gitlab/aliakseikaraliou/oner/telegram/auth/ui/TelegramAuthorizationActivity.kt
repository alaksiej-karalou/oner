package com.gitlab.aliakseikaraliou.oner.telegram.auth.ui

import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import com.gitlab.aliakseikaraliou.oner.base.ui.ConversationListActivity
import com.gitlab.aliakseikaraliou.oner.base.ui.base.SingleFragmentActivity
import com.gitlab.aliakseikaraliou.oner.telegram.auth.di.components.DaggerTelegramAuthorizationComponent
import com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.fragments.TelegramAuthorizationCodeFragment
import com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.fragments.TelegramAuthorizationPhoneFragment
import com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.navigators.TelegramAuthorizationNavigator
import com.gitlab.aliakseikaraliou.oner.telegram.auth.viewmodel.TelegramAuthorizationViewModel
import com.gitlab.aliakseikaraliou.oner.telegram.auth.viewmodel.factory.TelegramAuthorizationViewModelFactory
import javax.inject.Inject

class TelegramAuthorizationActivity : SingleFragmentActivity(), TelegramAuthorizationNavigator {
    lateinit var viewModel: TelegramAuthorizationViewModel

    @Inject
    lateinit var factory: TelegramAuthorizationViewModelFactory

    @Inject
    lateinit var phoneFragment: TelegramAuthorizationPhoneFragment

    @Inject
    lateinit var codeFragment: TelegramAuthorizationCodeFragment

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initDi()

        viewModel = ViewModelProviders.of(this, factory)
                .get(TelegramAuthorizationViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()

        viewModel.navigator = this
        viewModel.initializeAuthorizator()
    }

    private fun initDi() {
        val authorizationComponent = DaggerTelegramAuthorizationComponent.create()
        authorizationComponent.inject(this)
        authorizationComponent.inject(phoneFragment)
        authorizationComponent.inject(codeFragment)
    }

    override fun openPhoneFragment() {
        openFragment(phoneFragment)
    }

    override fun openCodeFragment() {
        openFragment(codeFragment)
    }

    override fun openMainActivity() {
        startActivity(Intent(this, ConversationListActivity::class.java))
    }
}