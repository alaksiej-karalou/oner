package com.gitlab.aliakseikaraliou.oner.telegram.models.clean

data class TelegramChatKey(val chatId: Long,
                           val chatOrder: Long) {
    companion object {
        fun default() = TelegramChatKey(chatOrder = Long.MAX_VALUE,
                chatId = 0)
    }
}