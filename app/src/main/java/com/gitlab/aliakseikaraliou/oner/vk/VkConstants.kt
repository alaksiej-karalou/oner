package com.gitlab.aliakseikaraliou.oner.vk

object VkConstants {

    object Json {
        const val CONVERSATION = "conversation"
        const val COUNT = "count"
        const val DATE = "date"
        const val FIRST_NAME = "first_name"
        const val FROM_ID = "from_id"
        const val ERROR = "error"
        const val ERROR_CODE = "error_code"
        const val ERROR_MSG = "error_msg"
        const val ID = "id"
        const val IN_READ = "in_read"
        const val ITEMS = "items"
        const val LAST_MESSAGE = "last_message"
        const val LAST_NAME = "last_name"
        const val LAST_SEEN = "last_seen"
        const val NAME = "name"
        const val ONLINE = "online"
        const val ONLINE_MOBILE = "online_mobile"
        const val OUT = "out"
        const val OUT_READ = "out_read"
        const val PEER = "peer"
        const val PEER_ID = "peer_id"
        const val PHOTO_100 = "photo_100"
        const val RESPONSE = "response"
        const val SCREEN_NAME = "screen_name"
        const val TEXT = "text"
        const val TIME = "time"
        const val TITLE = "title"
        const val UNREAD_COUNT = "unread_count"
    }

    object Queries {
        const val ACCESS_TOKEN = "access_token"
        const val LAST_SEEN = "last_seen"
        const val ONLINE = "online"
        const val PHOTO_100 = "photo_100"
        const val SCREEN_NAME = "screen_name"
        const val VERSION = "v"
    }

    const val CHAT_OFFSET = 2000000000

}