package com.gitlab.aliakseikaraliou.oner.telegram.execution.executables

import com.gitlab.aliakseikaraliou.oner.telegram.execution.TelegramExecutor
import org.drinkless.td.libcore.telegram.TdApi

abstract class BaseTelegramExecutable<API : TdApi.Object, T> {

    abstract val function: TdApi.Function

    abstract fun convertData(it: API): T

    suspend fun execute() = TelegramExecutor.execute(this)
}
