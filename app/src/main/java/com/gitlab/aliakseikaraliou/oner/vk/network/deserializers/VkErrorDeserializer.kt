package com.gitlab.aliakseikaraliou.oner.vk.network.deserializers

import com.gitlab.aliakseikaraliou.oner.vk.VkConstants
import com.gitlab.aliakseikaraliou.oner.vk.utils.exceptions.VkErrorCodes
import com.gitlab.aliakseikaraliou.oner.vk.utils.exceptions.VkException
import com.gitlab.aliakseikaraliou.oner.vk.utils.exceptions.VkTooManyRequestsException
import com.google.gson.JsonElement

class VkErrorDeserializer {
    fun deserialize(json: JsonElement?): VkException {
        json?.asJsonObject?.get(VkConstants.Json.ERROR)?.asJsonObject?.also { jsonObject ->
            val errorCode = jsonObject.get(VkConstants.Json.ERROR_CODE)?.asInt
            val errorMessage = jsonObject.get(VkConstants.Json.ERROR_MSG)?.asString

            return when (errorCode) {
                VkErrorCodes.TOO_MANY_REQUESTS -> VkTooManyRequestsException(errorMessage)
                else -> VkException(errorCode, errorMessage)
            }
        }
        return VkException(-1, "Unknown exception")
    }

}