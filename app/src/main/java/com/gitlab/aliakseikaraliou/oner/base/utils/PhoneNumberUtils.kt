package com.gitlab.aliakseikaraliou.oner.base.utils

import java.util.regex.Pattern

object PhoneNumberUtils {
    private val pattern = Pattern.compile("\\+?[\\d]{4,16}")
    
    fun isValid(phoneNumber: String) = pattern.matcher(phoneNumber).matches()
}