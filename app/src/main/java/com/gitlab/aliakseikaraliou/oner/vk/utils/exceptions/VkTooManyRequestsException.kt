package com.gitlab.aliakseikaraliou.oner.vk.utils.exceptions

class VkTooManyRequestsException(override val message: String?) : VkException(VkErrorCodes.TOO_MANY_REQUESTS, message)