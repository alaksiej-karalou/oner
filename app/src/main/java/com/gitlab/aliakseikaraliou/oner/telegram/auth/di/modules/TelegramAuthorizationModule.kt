package com.gitlab.aliakseikaraliou.oner.telegram.auth.di.modules

import com.gitlab.aliakseikaraliou.oner.telegram.auth.models.TelegramAuthorizator
import com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.fragments.TelegramAuthorizationCodeFragment
import com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.fragments.TelegramAuthorizationPhoneFragment
import com.gitlab.aliakseikaraliou.oner.telegram.auth.viewmodel.factory.TelegramAuthorizationViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class TelegramAuthorizationModule {

    @Singleton
    @Provides
    fun providesAuthorizator(): TelegramAuthorizator = TelegramAuthorizator()

    @Singleton
    @Provides
    fun providesViewModelFactory(authorizator: TelegramAuthorizator) =
            TelegramAuthorizationViewModelFactory(authorizator)

    @Singleton
    @Provides
    fun providesPhoneFragment() = TelegramAuthorizationPhoneFragment()

    @Singleton
    @Provides
    fun providesCodeFragment() = TelegramAuthorizationCodeFragment()
}