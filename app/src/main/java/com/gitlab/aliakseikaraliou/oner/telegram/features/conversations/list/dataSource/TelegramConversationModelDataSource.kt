package com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.dataSource

import androidx.paging.PageKeyedDataSource
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.telegram.db.repositories.TelegramConversationModelRepository
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramChatKey
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.conversations.TelegramConversation
import kotlinx.coroutines.experimental.launch

class TelegramConversationModelDataSource(val repository: TelegramConversationModelRepository) : PageKeyedDataSource<TelegramChatKey, Conversation>() {

    override fun loadInitial(params: LoadInitialParams<TelegramChatKey>, callback: LoadInitialCallback<TelegramChatKey, Conversation>) {
        launch {
            val conversations = repository.loadConversations(count = params.requestedLoadSize).conversations
            callback.onResult(conversations, null, createChatKeyOf(conversations.last()))
        }
    }

    override fun loadAfter(params: LoadParams<TelegramChatKey>, callback: LoadCallback<TelegramChatKey, Conversation>) {
        launch {
            val conversations = repository.loadConversations(count = params.requestedLoadSize, key = params.key).conversations

            val key = conversations.takeIf { it.isNotEmpty() }?.let { createChatKeyOf(it.last()) }
            callback.onResult(conversations, key)
        }
    }

    override fun loadBefore(params: LoadParams<TelegramChatKey>,
                            callback: LoadCallback<TelegramChatKey, Conversation>) {
    }

    private fun createChatKeyOf(item: TelegramConversation) = TelegramChatKey(item.id, item.order)
}