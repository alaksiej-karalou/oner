package com.gitlab.aliakseikaraliou.oner.vk.network.deserializers

import com.gitlab.aliakseikaraliou.oner.base.utils.dateFromTimestamp
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.VkOnlineStatus
import com.gitlab.aliakseikaraliou.oner.base.utils.extension.getData
import com.gitlab.aliakseikaraliou.oner.vk.VkConstants
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkUserList
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkUser
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class VkUserListDeserializer : BaseVkDeserializer<VkUserList>() {
    override fun deserialize(json: JsonElement): VkUserList {
        val list = json.asJsonArray.map {
            val jsonObject = it.asJsonObject

            val userId = jsonObject.getData<Long>(VkConstants.Json.ID)
            val firstName = jsonObject.getData<String>(VkConstants.Json.FIRST_NAME)
            val lastName = jsonObject.getData<String>(VkConstants.Json.LAST_NAME)
            val username = jsonObject.getData<String?>(VkConstants.Json.SCREEN_NAME)
            val photo50 = jsonObject.getData<String?>(VkConstants.Json.PHOTO_100)

            val onlineStatus = parseOnlineStatus(jsonObject)

            VkUser(id = userId,
                    firstName = firstName,
                    lastName = lastName,
                    username = username,
                    imageUrl = photo50,
                    onlineStatus = onlineStatus)
        }

        return VkUserList(list)
    }

    private fun parseOnlineStatus(jsonObject: JsonObject): VkOnlineStatus {
        val onlineMobile = jsonObject.getData<Int?>(VkConstants.Json.ONLINE_MOBILE)?.let { it > 0 }
                ?: false
        val online = jsonObject.getData<Int?>(VkConstants.Json.ONLINE)?.let { it > 0 }
                ?: false

        return when {
            onlineMobile -> VkOnlineStatus.mobile()
            online -> VkOnlineStatus.web()
            else -> {
                val date = jsonObject.getData<JsonObject?>(VkConstants.Json.LAST_SEEN)?.getData<Long?>(VkConstants.Json.TIME)?.let {
                    dateFromTimestamp(it)
                }

                VkOnlineStatus.offline(date)
            }
        }

    }
}