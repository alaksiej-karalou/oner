package com.gitlab.aliakseikaraliou.oner.telegram.auth.models

import com.gitlab.aliakseikaraliou.oner.BuildConfig
import com.gitlab.aliakseikaraliou.oner.base.OnerConfiguration
import com.gitlab.aliakseikaraliou.oner.telegram.TelegramConfiguration
import com.gitlab.aliakseikaraliou.oner.telegram.execution.TelegramExecutor
import org.drinkless.td.libcore.telegram.Client
import org.drinkless.td.libcore.telegram.TdApi

class TelegramAuthorizator {
    private var currentState: TdApi.AuthorizationState? = null

    private val updateHandler = Client.ResultHandler { tdApiObject ->
        if (tdApiObject is TdApi.UpdateAuthorizationState) {
            currentState = tdApiObject.authorizationState

            when (tdApiObject.authorizationState) {
                is TdApi.AuthorizationStateWaitTdlibParameters -> sendParameters()
                is TdApi.AuthorizationStateWaitEncryptionKey -> sendEncryptionKey()
                is TdApi.AuthorizationStateWaitPhoneNumber -> sendPhoneNumber()
                is TdApi.AuthorizationStateWaitCode -> sendCode()
                is TdApi.AuthorizationStateReady -> sendReady()
            }
        }
    }

    private val exceptionHandler = Client.ExceptionHandler {
    }

    private lateinit var client: Client

    fun initialize() {
        client = Client.create(updateHandler, exceptionHandler, exceptionHandler)
    }

    var phoneNumber: String? = null
        set(value) {
            field = value

            sendPhoneNumber()
        }

    var code: String? = null
        set(value) {
            field = value

            sendCode()
        }


    var onAuthorizationStateListener: ((state: TelegramAuthorizationState) -> Unit)? = null

    private fun sendParameters() {
        val parameters = TdApi.TdlibParameters().apply {
            databaseDirectory = "${OnerConfiguration.applicationDir}/db/td"
            useMessageDatabase = true
            useSecretChats = true
            apiId = TelegramConfiguration.apiId
            apiHash = TelegramConfiguration.hashId
            systemLanguageCode = "en"
            deviceModel = "Desktop"
            systemVersion = "Android"
            applicationVersion = BuildConfig.VERSION_NAME
            enableStorageOptimizer = true
        }

        client.send(TdApi.SetTdlibParameters(parameters), updateHandler, exceptionHandler)
    }

    private fun sendEncryptionKey() {
        client.send(TdApi.CheckDatabaseEncryptionKey(), updateHandler, exceptionHandler)
    }

    private fun sendPhoneNumber() {
        if (phoneNumber == null) {
            onAuthorizationStateListener?.invoke(TelegramAuthorizationState.ENTER_PHONE_NUMBER)
        } else {
            val authenticationPhoneNumber = TdApi.SetAuthenticationPhoneNumber().apply {
                phoneNumber = this@TelegramAuthorizator.phoneNumber
                allowFlashCall = true
                isCurrentPhoneNumber = true
            }

            client.send(authenticationPhoneNumber, updateHandler, exceptionHandler)
        }
    }

    private fun sendCode() {
        onAuthorizationStateListener?.invoke(TelegramAuthorizationState.ENTER_CODE)

        if (code == null) {
            onAuthorizationStateListener?.invoke(TelegramAuthorizationState.ENTER_CODE)
        } else if (currentState is TdApi.AuthorizationStateWaitCode) {
            val authenticationCode = TdApi.CheckAuthenticationCode().apply {
                code = this@TelegramAuthorizator.code
            }

            client.send(authenticationCode, updateHandler, exceptionHandler)
        }
    }

    private fun sendReady() {
        TelegramExecutor.init(client)
        onAuthorizationStateListener?.invoke(TelegramAuthorizationState.READY)
    }
}