package com.gitlab.aliakseikaraliou.oner.base.ui.base

import com.gitlab.aliakseikaraliou.oner.R

abstract class SingleFragmentActivity : BaseActivity() {
    final override val layoutId: Int
        get() = R.layout.activity_fragment

    val currentFragment: BaseFragment
        get() = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG) as BaseFragment

    fun openFragment(fragment: BaseFragment, addToBackStack: Boolean = false) {
        val fragmentTransaction = supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment, FRAGMENT_TAG)

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null)
        }

        fragmentTransaction
                .commit()
    }

    companion object {
        private const val FRAGMENT_TAG = "fragment tag"
    }
}