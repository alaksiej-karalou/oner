package com.gitlab.aliakseikaraliou.oner.telegram.execution

import com.gitlab.aliakseikaraliou.oner.telegram.execution.exception.TelegramApiException
import com.gitlab.aliakseikaraliou.oner.telegram.execution.exception.TelegramException
import com.gitlab.aliakseikaraliou.oner.telegram.execution.executables.BaseTelegramExecutable
import org.drinkless.td.libcore.telegram.Client
import org.drinkless.td.libcore.telegram.TdApi
import kotlin.coroutines.experimental.suspendCoroutine

internal object TelegramExecutor {
    var client: Client? = null

    fun init(client: Client) {
        this.client = client
    }

    @Suppress("UNCHECKED_CAST")
    suspend fun <API : TdApi.Object, T> execute(executable: BaseTelegramExecutable<API, T>): T {
        return suspendCoroutine { continuation ->
            client?.send(executable.function) { result ->
                if (result is TdApi.Error) {
                    continuation.resumeWithException(TelegramApiException(result))
                } else {
                    try {
                        val data = executable.convertData(result as API)
                        continuation.resume(data)
                    } catch (throwable: Throwable) {
                        continuation.resumeWithException(TelegramException(throwable))
                    }
                }
            }
        }


    }
}
