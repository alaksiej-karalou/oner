package com.gitlab.aliakseikaraliou.oner.sms.models.clean.authors

data class SmsContact(override val id: Long,
                 override val fullName: String,
                 val phoneNumber: String,
                 override val imageUrl: String?) : SmsAuthor
