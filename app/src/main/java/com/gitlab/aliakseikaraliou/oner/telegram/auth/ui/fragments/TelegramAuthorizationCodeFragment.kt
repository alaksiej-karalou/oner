package com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.fragments

import android.os.Bundle
import android.view.View
import com.gitlab.aliakseikaraliou.oner.R
import kotlinx.android.synthetic.main.fragment_telegram_auth_code.*

class TelegramAuthorizationCodeFragment : BaseTelegramAuthorizationFragment() {
    override val layoutId: Int
        get() = R.layout.fragment_telegram_auth_code

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button.setOnClickListener {
            val code = codeView.text.toString()
            viewModel.authorizator.code = code
        }
    }
}