package com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.utils.diffUtils

import androidx.recyclerview.widget.DiffUtil
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation

class ConversationDiffUtilsItemsCallback : DiffUtil.ItemCallback<Conversation>() {
    override fun areItemsTheSame(oldItem: Conversation, newItem: Conversation) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Conversation, newItem: Conversation) = oldItem == newItem
}