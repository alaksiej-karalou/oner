package com.gitlab.aliakseikaraliou.oner.vk.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.gitlab.aliakseikaraliou.oner.vk.db.entities.VkChatEntity

@Suppress("unused")
@Dao
interface VkChatDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: VkChatEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: List<VkChatEntity>)
}