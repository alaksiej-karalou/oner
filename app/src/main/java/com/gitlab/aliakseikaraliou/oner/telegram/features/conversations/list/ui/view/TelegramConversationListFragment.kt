package com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.ui.view

import android.os.Bundle
import android.view.View
import com.gitlab.aliakseikaraliou.oner.R
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.di.components.ConversationListComponent
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.viewModels.ConversationListPagedViewModel
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.views.ConversationListFragment
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.utils.adapters.adapter.ConversationListPagedAdapter
import com.gitlab.aliakseikaraliou.oner.base.models.ConversationModel
import javax.inject.Inject

class TelegramConversationListFragment : ConversationListFragment() {

    @Inject
    override lateinit var viewModel: ConversationListPagedViewModel

    override val layoutId = R.layout.fragment_dialog_list

    override val themeId = R.style.TelegramTheme

    override fun setupViewModel(conversationListComponent: ConversationListComponent) {
        super.setupViewModel(conversationListComponent)

        conversationListComponent
                .telegram()
                .fragment(this)
                .build()
                .inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadConversations()
    }
}