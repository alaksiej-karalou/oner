package com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.navigators

interface TelegramAuthorizationNavigator {
    fun openPhoneFragment()
    fun openCodeFragment()
    fun openMainActivity()
}