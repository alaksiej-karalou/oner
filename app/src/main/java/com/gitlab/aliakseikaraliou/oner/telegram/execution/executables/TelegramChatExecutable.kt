package com.gitlab.aliakseikaraliou.oner.telegram.execution.executables

import com.gitlab.aliakseikaraliou.oner.telegram.converters.TelegramChatConverter
import com.gitlab.aliakseikaraliou.oner.telegram.converters.TelegramConversationConverter
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.authors.TelegramDraftChat
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations.TelegramDraftConversation
import org.drinkless.td.libcore.telegram.TdApi

class TelegramChatExecutable(chatId: Long)
    : BaseTelegramExecutable<TdApi.Chat, TelegramDraftChat>() {
    override val function = TdApi.GetChat(chatId)
    override fun convertData(it: TdApi.Chat) = TelegramChatConverter.convert(it)
}
