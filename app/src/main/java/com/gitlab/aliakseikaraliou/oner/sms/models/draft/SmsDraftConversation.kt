package com.gitlab.aliakseikaraliou.oner.sms.models.draft

data class SmsDraftConversation(var lastMessage: SmsDraftMessage,
                                val threadId: Int,
                                var totalCount: Int = 0,
                                var unreadCount: Int = 0)