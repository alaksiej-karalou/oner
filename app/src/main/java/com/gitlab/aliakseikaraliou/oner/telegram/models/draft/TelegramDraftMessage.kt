package com.gitlab.aliakseikaraliou.oner.telegram.models.draft

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramMessage
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramAuthor
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramChat
import java.util.Date

data class TelegramDraftMessage(val id: Long,
                                val text: String?,
                                val authorId: Long?,
                                val chatId: Long = 0,
                                val date: Date,
                                val signature: String,
                                val isRead: Boolean,
                                val isOut: Boolean) {

    fun convert(author: TelegramAuthor?, chat: TelegramChat? = null): TelegramMessage {
        if (author?.id != authorId || chatId != 0L && chatId != chat?.id) {
            throw IllegalArgumentException()
        }

        return TelegramMessage(id = id,
                text = text,
                author = author,
                chat = chat,
                date = date,
                signature = signature,
                isRead = isRead,
                isOut = isOut)
    }
}