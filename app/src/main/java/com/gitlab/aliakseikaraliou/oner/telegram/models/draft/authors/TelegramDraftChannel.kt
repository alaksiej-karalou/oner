package com.gitlab.aliakseikaraliou.oner.telegram.models.draft.authors

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramChannel

class TelegramDraftChannel(val id: Long,
                           val fullName: String,
                           val imageId: Int?,
                           val imageUrl: String?) {
    fun build(imageUrl: String? = null): TelegramChannel {
        val imagePath = imageUrl ?: this.imageUrl
    
        val fullImagePath = imagePath?.let { "file://$it" }
        
        return TelegramChannel(id = id,
                fullName = fullName,
                imageUrl = fullImagePath)
    }
}