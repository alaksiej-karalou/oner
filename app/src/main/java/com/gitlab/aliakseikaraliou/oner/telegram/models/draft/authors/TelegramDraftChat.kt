package com.gitlab.aliakseikaraliou.oner.telegram.models.draft.authors

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramChat

class TelegramDraftChat(val id: Long,
                        val fullName: String,
                        val imageId: Int?,
                        val imageUrl: String?) {
    fun build(imageUrl: String? = null): TelegramChat {
        val imagePath = imageUrl
                ?: this.imageUrl
    
        val fullImagePath = imagePath?.let { "file://$it" }
        
        return TelegramChat(id = id,
                fullName = fullName,
                imageUrl = fullImagePath)
    }
    
}