package com.gitlab.aliakseikaraliou.oner.vk.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkUser

@Entity(tableName = "users")
data class VkUserEntity(val firstName: String,
                        val lastName: String,
                        val username: String?,
                        @PrimaryKey val id: Long,
                        val imageUrl: String?) {

    constructor(user: VkUser) : this(firstName = user.firstName,
            lastName = user.lastName,
            username = user.username,
            id = user.id,
            imageUrl = user.imageUrl)

}