package com.gitlab.aliakseikaraliou.oner.vk.utils

import com.gitlab.aliakseikaraliou.oner.vk.VkConstants

object VkPeerer {
    fun isUserId(id: Long) = id > 0 && id <= VkConstants.CHAT_OFFSET
    
    fun isChatId(id: Long) = id > VkConstants.CHAT_OFFSET
    
    fun isChannelId(id: Long) = id < 0
    
    fun peerIdToChatId(peerId: Long) = peerId
            .takeIf { peerId > VkConstants.CHAT_OFFSET }
            ?.let { peerId - VkConstants.CHAT_OFFSET }
            ?: throw IllegalArgumentException("Failed to parse peer id with value $peerId to chat id")
    
    fun chatIdToPeerId(chatId: Long) = chatId
            .takeIf { chatId > 0 && chatId < VkConstants.CHAT_OFFSET }
            ?.let { chatId + VkConstants.CHAT_OFFSET }
            ?: throw IllegalArgumentException("Failed to parse chat id with value $chatId to peer id")
    
    fun peerIdToUserId(peerId: Long) = peerId
            .takeIf { peerId < VkConstants.CHAT_OFFSET && peerId > 0 }
            ?: throw IllegalArgumentException("Failed to parse peer id with value $peerId to user id")
    
    fun userIdToPeerId(userId: Long) = userId
            .takeIf { userId < VkConstants.CHAT_OFFSET && userId > 0 }
            ?: throw IllegalArgumentException("Failed to parse user id with value $userId to peer id")
    
    fun peerIdToChannelId(peerId: Long) = peerId
            .takeIf { peerId < 0 }
            ?.let { -peerId }
            ?: throw IllegalArgumentException("Failed to parse peer id with value $peerId to channel id")
    
    fun channelIdToPeerId(channelId: Long) = channelId
            .takeIf { channelId > 0 }
            ?.let { -channelId }
            ?: throw IllegalArgumentException("Failed to parse channel id with value $channelId to peer id")
}