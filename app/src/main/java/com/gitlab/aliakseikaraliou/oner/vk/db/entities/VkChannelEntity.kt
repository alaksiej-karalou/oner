package com.gitlab.aliakseikaraliou.oner.vk.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkChannel

@Entity(tableName = "channels")
data class VkChannelEntity(@PrimaryKey val id: Long,
                        val fullName: String,
                        val imageUrl: String?) {

    constructor(channel: VkChannel) : this(id = channel.id,
            fullName = channel.fullName,
            imageUrl = channel.imageUrl)

}