package com.gitlab.aliakseikaraliou.oner.telegram.models.clean.conversations

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramMessage
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramUser

data class TelegramPrivateConversation(override val receiver: TelegramUser,
                                       override val lastMessage: TelegramMessage?,
                                       val isSecret: Boolean,
                                       override val isPinned: Boolean,
                                       override val unreadCount: Int,
                                       override val order: Long) : TelegramConversation