package com.gitlab.aliakseikaraliou.oner.telegram.models.clean

import com.gitlab.aliakseikaraliou.oner.base.models.ConversationModel
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.conversations.TelegramConversation

data class TelegramConversationModel(override val conversations: List<TelegramConversation>) : ConversationModel