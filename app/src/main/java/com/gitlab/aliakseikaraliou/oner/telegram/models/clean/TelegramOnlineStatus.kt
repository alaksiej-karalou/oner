package com.gitlab.aliakseikaraliou.oner.telegram.models.clean

import java.util.*

data class TelegramOnlineStatus private constructor(val status: Status,
                                                    val wasOnline: Date? = null) {
    enum class Status {
        ONLINE,
        OFFLINE
    }

    fun isOnline() = status == Status.ONLINE

    companion object {
        fun online() = TelegramOnlineStatus(Status.ONLINE)

        fun offline(date: Date? = null) = TelegramOnlineStatus(Status.OFFLINE, date)
    }
}