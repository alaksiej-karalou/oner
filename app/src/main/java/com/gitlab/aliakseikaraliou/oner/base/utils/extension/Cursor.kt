package com.gitlab.aliakseikaraliou.oner.base.utils.extension

import android.database.Cursor

fun Cursor.toMap(): Map<String, String?> {
    val map = mutableMapOf<String, String?>()
    
    for (i in 0 until columnCount) {
        map[getColumnName(i)] = getString(i)
    }
    
    return map
}

@Suppress("IMPLICIT_CAST_TO_ANY")
inline fun <reified T> Cursor.get(columnName: String): T {
    val index = getColumnIndexOrThrow(columnName)
    
    return when (T::class) {
        Boolean::class -> getInt(index) != 0
        Double::class -> getDouble(index)
        Float::class -> getFloat(index)
        Int::class -> getInt(index)
        Long::class -> getLong(index)
        Short::class -> getShort(index)
        String::class -> getString(index)
        else -> throw IllegalArgumentException("Unsupported cursor type")
    } as T
}
