package com.gitlab.aliakseikaraliou.oner.base.di.components

import android.content.Context
import com.gitlab.aliakseikaraliou.oner.base.db.BaseRepositoryFactory
import com.gitlab.aliakseikaraliou.oner.base.di.modules.ContextModule
import com.gitlab.aliakseikaraliou.oner.base.di.modules.RepositoryModule
import com.gitlab.aliakseikaraliou.oner.base.di.scopes.AppScope
import com.gitlab.aliakseikaraliou.oner.vk.di.modules.VkModule
import com.gitlab.aliakseikaraliou.oner.vk.BaseVkModel
import dagger.Component

@AppScope
@Component(modules = [ContextModule::class,
    RepositoryModule::class,
    VkModule::class])

interface AppComponent {
    val context: Context

    val repositoryFactory: BaseRepositoryFactory

    val vkModel: BaseVkModel
}