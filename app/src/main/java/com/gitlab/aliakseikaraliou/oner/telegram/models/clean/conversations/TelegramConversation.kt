package com.gitlab.aliakseikaraliou.oner.telegram.models.clean.conversations

import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramReceiver

interface  TelegramConversation : Conversation {
    override val receiver: TelegramReceiver
    val order: Long
}