package com.gitlab.aliakseikaraliou.oner.vk.network.deserializers

import com.gitlab.aliakseikaraliou.oner.base.utils.dateFromTimestamp
import com.gitlab.aliakseikaraliou.oner.base.utils.extension.getData
import com.gitlab.aliakseikaraliou.oner.vk.VkConstants
import com.gitlab.aliakseikaraliou.oner.vk.models.draft.VkDraftConversation
import com.gitlab.aliakseikaraliou.oner.vk.models.draft.VkDraftConversationModel
import com.gitlab.aliakseikaraliou.oner.vk.models.draft.VkDraftMessage
import com.gitlab.aliakseikaraliou.oner.vk.utils.VkPeerer
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class VkDraftConversationModelDeserializer : BaseVkDeserializer<VkDraftConversationModel>() {
    override fun deserialize(json: JsonElement): VkDraftConversationModel {
        val jsonObject = json.asJsonObject

        val count = jsonObject.getData<Int>(VkConstants.Json.COUNT)
        val items = jsonObject.getData<JsonArray>(VkConstants.Json.ITEMS)

        val list = mutableListOf<VkDraftConversation>()

        items.forEach { jsonElement ->
            val conversationObject = jsonElement.asJsonObject
                    ?.getData<JsonObject>(VkConstants.Json.CONVERSATION)

            val peerId = conversationObject
                    ?.getData<JsonObject>(VkConstants.Json.PEER)
                    ?.getData<Long>(VkConstants.Json.ID)

            val unreadCount = jsonElement.asJsonObject
                    ?.getData(VkConstants.Json.UNREAD_COUNT) ?: 0

            val inRead = conversationObject?.asJsonObject
                    ?.getData(VkConstants.Json.IN_READ) ?: Long.MAX_VALUE

            val outRead = conversationObject?.asJsonObject
                    ?.getData(VkConstants.Json.OUT_READ) ?: Long.MAX_VALUE

            val lastMessage = jsonElement.asJsonObject
                    ?.getData<JsonObject>(VkConstants.Json.LAST_MESSAGE)
                    ?.let { parseMessage(it, inRead, outRead) }

            if (peerId != null && lastMessage != null) {
                val draftConversation = VkDraftConversation(peerId = peerId,
                        lastMessage = lastMessage,
                        unreadCount = unreadCount)

                list.add(draftConversation)
            }
        }

        return VkDraftConversationModel(count = count,
                conversations = list)
    }

    private fun parseMessage(jsonObject: JsonObject, inRead: Long, outRead: Long): VkDraftMessage {
        val id = jsonObject.getData<Long>(VkConstants.Json.ID)
        val date = dateFromTimestamp(jsonObject.getData(VkConstants.Json.DATE))
        val text = jsonObject.getData<String>(VkConstants.Json.TEXT)
        val isOut = jsonObject.getData<Int>(VkConstants.Json.OUT) > 0

        val fromId = jsonObject.getData<Long>(VkConstants.Json.FROM_ID)
        val peerId = jsonObject.getData<Long>(VkConstants.Json.PEER_ID)

        val authorId: Long
        val chatId: Long?

        if (VkPeerer.isChatId(peerId)) {
            authorId = fromId
            chatId = peerId
        } else {
            authorId = peerId
            chatId = null
        }

        val isRead = if (isOut) id <= outRead else id <= inRead

        return VkDraftMessage(id = id,
                text = text,
                authorId = authorId,
                date = date,
                chatId = chatId,
                isRead = isRead,
                isOut = !isOut)
    }
}