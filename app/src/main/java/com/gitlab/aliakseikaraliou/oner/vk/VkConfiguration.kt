package com.gitlab.aliakseikaraliou.oner.vk

object VkConfiguration {
    const val API_HOST = "https://api.vk.com/method/"
    const val API_VERSION = 5.84
    const val ACCESS_TOKEN = "b9fbe6265971a5395964675c50ebf59cadf476d39f5fbd52b736ebf9649fdadefe756e92f41525e24fa9a"
    
    const val INITIAL_PAGE_SIZE = 100
    const val PAGE_SIZE = 20
    const val DELAY_IN_MILLIS: Long = 350
    const val DB_NAME = "vk"
}