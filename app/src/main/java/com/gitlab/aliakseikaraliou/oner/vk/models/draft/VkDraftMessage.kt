package com.gitlab.aliakseikaraliou.oner.vk.models.draft

import com.gitlab.aliakseikaraliou.oner.vk.models.clean.VkMessage
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkAuthor
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkChat
import java.util.*

data class VkDraftMessage(val id: Long,
                          val text: String?,
                          val authorId: Long,
                          val chatId: Long?,
                          val date: Date,
                          val isRead: Boolean,
                          val isOut: Boolean) {
    fun create(author: VkAuthor, chat: VkChat? = null): VkMessage {
        if (authorId != author.peerId || chatId != chat?.peerId) {
            throw IllegalArgumentException()
        }

        return VkMessage(id = id,
                text = text,
                date = date,
                author = author,
                chat = chat,
                isRead = isRead,
                isOut = isOut
        )

    }
}