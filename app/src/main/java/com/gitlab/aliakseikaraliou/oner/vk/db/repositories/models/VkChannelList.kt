package com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models

import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkChannel

data class VkChannelList(val list: List<VkChannel>)