package com.gitlab.aliakseikaraliou.oner.vk.features.conversations.di.modules

import androidx.lifecycle.ViewModelProviders
import com.gitlab.aliakseikaraliou.oner.base.db.BaseRepositoryFactory
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.viewModels.ConversationListPagedViewModel
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.VkConversationModelRepository
import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.di.scopes.VkListScope
import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.ui.view.VkConversationListFragment
import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.ui.viewModel.VkConversationListViewModel
import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.ui.viewModel.VkConversationListViewModelFactory
import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.dataSource.VkConversationModelDataSourceFactory
import dagger.Module
import dagger.Provides

@Module
class VkListModule {

    @VkListScope
    @Provides
    fun provideRepository(databaseFactory: BaseRepositoryFactory) = databaseFactory
            .vk
            .conversationModelRepository

    @VkListScope
    @Provides
    fun provideDataSourceFactory(repository: VkConversationModelRepository) = VkConversationModelDataSourceFactory(repository)

    @VkListScope
    @Provides
    fun provideViewModel(fragment: VkConversationListFragment,
                         dataSource: VkConversationModelDataSourceFactory): ConversationListPagedViewModel {
        val viewModelFactory = VkConversationListViewModelFactory(dataSource)

        return ViewModelProviders.of(fragment, viewModelFactory).get(VkConversationListViewModel::class.java)
    }
}