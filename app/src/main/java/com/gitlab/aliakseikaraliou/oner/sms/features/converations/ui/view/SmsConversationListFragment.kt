package com.gitlab.aliakseikaraliou.oner.sms.features.converations.ui.view

import android.Manifest
import android.os.Bundle
import android.view.View
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.di.components.ConversationListComponent
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.viewModels.ConversationListPagedViewModel
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.views.ConversationListFragment
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener
import javax.inject.Inject

class SmsConversationListFragment : ConversationListFragment() {
    @Inject
    override lateinit var viewModel: ConversationListPagedViewModel

    override fun setupViewModel(conversationListComponent: ConversationListComponent) {
        super.setupViewModel(conversationListComponent)

        conversationListComponent
                .sms()
                .fragment(this)
                .build()
                .inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requestPermissions { permissionGranted ->
            if (permissionGranted) {
                loadConversations()
            } else {
                //TODO error handling
            }
        }
    }

    private fun requestPermissions(readPermissionGranted: (Boolean) -> Unit) {
        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.READ_SMS,
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_CONTACTS)
                .withListener(object : BaseMultiplePermissionsListener() {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        report?.let { permissionReport ->
                            val isReadSmsPermissionGranted = permissionReport.grantedPermissionResponses
                                    .map { it.permissionName }
                                    .contains(Manifest.permission.READ_SMS)
                            readPermissionGranted(isReadSmsPermissionGranted)
                        }
                    }
                })
                .check()
    }
}