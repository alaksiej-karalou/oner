package com.gitlab.aliakseikaraliou.oner.sms.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.base.models.Channel

data class SmsChannel(override val id: Long,
                      override val fullName: String) : Channel, SmsAuthor {
    
    override val imageUrl = null
}
