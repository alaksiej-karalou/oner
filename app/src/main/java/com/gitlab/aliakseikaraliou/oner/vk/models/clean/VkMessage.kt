package com.gitlab.aliakseikaraliou.oner.vk.models.clean

import com.gitlab.aliakseikaraliou.oner.base.models.Chat
import com.gitlab.aliakseikaraliou.oner.base.models.Message
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkAuthor
import java.util.Date

data class VkMessage(override val id: Long,
                     override val text: String?,
                     override val author: VkAuthor?,
                     override val chat: Chat?,
                     override val date: Date,
                     override val isRead: Boolean,
                     override val isOut: Boolean) : Message