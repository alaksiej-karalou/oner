package com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.di.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class TelegramListScope